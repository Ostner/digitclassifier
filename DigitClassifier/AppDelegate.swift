//
//  AppDelegate.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 6/25/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions options: [UIApplicationLaunchOptionsKey: Any]?)
        -> Bool
    {
        window = UIWindow()
        window?.backgroundColor = .white
        let vc = ViewController()
        let navVc = UINavigationController(rootViewController: vc)
        navVc.navigationBar.prefersLargeTitles = true
        window?.rootViewController = navVc
        window?.makeKeyAndVisible()
        return true
    }


}

