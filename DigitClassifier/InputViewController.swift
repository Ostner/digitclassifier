//
//  InputViewController.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 7/9/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class InputViewController: UIViewController, UIViewControllerTransitioningDelegate {

    var image: UIImage? {
        didSet {
            (view as! UIImageView).image = image
        }
    }

    override func loadView() {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        view = imageView
    }

    override var modalPresentationStyle: UIModalPresentationStyle {
        get {
            return .custom
        }
        set {
            return
        }
    }

    override var transitioningDelegate: UIViewControllerTransitioningDelegate? {
        get {
            return self
        }
        set {
            return
        }
    }

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return InputPresentationController(presentedViewController: presented, presenting: presenting)
    }

}
