//
//  Extensions.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 7/7/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import CoreML
import UIKit

extension MLMultiArray {
    var argmax: Int? {
        guard count > 0 else { return nil }
        var arg = 0
        for index in 1 ..< count {
            if self[arg].compare(self[index]) == .orderedAscending {
                arg = index
            }
        }
        return arg
    }
}

extension UIImage {
    var alphaValues: [UInt8]? {
        let dataSize = Int(size.width * size.height)
        var pixelData = [UInt8](repeating: 0, count: dataSize)
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let context = CGContext(
            data: &pixelData,
            width: Int(size.width),
            height: Int(size.height),
            bitsPerComponent: 8,
            bytesPerRow: Int(size.width),
            space: colorSpace,
            bitmapInfo: CGImageAlphaInfo.alphaOnly.rawValue)
        context?.draw(cgImage!, in: CGRect(origin: .zero, size: size))
        return pixelData
    }
}
