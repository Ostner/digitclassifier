//
//  DigitPreprocessor.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 6/29/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class DigitPreprocessor {

    let original: UIImage
    var processed: [Float32] = []

    let digitSize = CGSize(width: 20, height: 20)
    let imageSize = CGSize(width: 28, height: 28)

    init(image: UIImage) {
        original = image
    }

    var inputImage: UIImage {
        let pixel = processed.map { UInt8($0*255)}
        return draw(pixelData: pixel)
    }

    func run() {
        let sizeAdapted = fitToDigitSize(image: original)
        let borderAdded = addBorder(image: sizeAdapted)
        processed = encode(image: borderAdded)
    }

    private func fitToDigitSize(image: UIImage) -> UIImage {
        let aspectRatio = image.size.width / image.size.height
        print(aspectRatio)
        var x: CGFloat = 0
        var y: CGFloat = 0
        var boundingBox = digitSize
        if aspectRatio < 1 {
            let aspectWidth = digitSize.width*aspectRatio
            x = (digitSize.width - aspectWidth) / 2
            boundingBox.width = aspectWidth
        } else {
            let aspectHeight = digitSize.height*pow(aspectRatio, -1)
            y = (digitSize.height - aspectHeight) / 2
            boundingBox.height = aspectHeight
        }
        print(x, y)
        let renderer = UIGraphicsImageRenderer(size: digitSize)
        return renderer.image { _ in
            image.draw(in: CGRect(origin: CGPoint(x: x, y: y), size: boundingBox))
        }
    }

    private func addBorder(image: UIImage) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: imageSize)
        return renderer.image { _ in
            image.draw(at: CGPoint(x: 4, y: 4))
        }
    }

    private func encode(image: UIImage) -> [Float32] {
        let result = image.alphaValues!.map { Float32($0) / 255 }
        var unraveled: [[Float32]] = []
        for m in 0..<28 {
            var row: [Float32] = []
            for n in 0..<28 {
                row.append(result[m*n + n]*255)
            }
            unraveled.append(row)
        }
        unraveled.forEach {
            print($0)
        }
        return result
    }

    private func draw(pixelData: [UInt8]) -> UIImage {
        let provider = CGDataProvider(
            dataInfo: nil,
            data: pixelData,
            size: Int(imageSize.width * imageSize.height),
            releaseData: {_, _, _ in })!
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let cgImage = CGImage(
            width: 28,
            height: 28,
            bitsPerComponent: 8,
            bitsPerPixel: 8,
            bytesPerRow: 28,
            space: colorSpace,
            bitmapInfo: .byteOrderMask,
            provider: provider,
            decode: nil,
            shouldInterpolate: false,
            intent: .defaultIntent)!
        return UIImage(cgImage: cgImage)

    }

}
