//
//  ViewController.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 6/25/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let canvas: CanvasView = {
        let view = CanvasView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        view.layer.borderWidth = 3
        view.layer.borderColor = UIColor.darkGray.cgColor
        view.backgroundColor = .white
        return view
    }()

    let prediction: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.layer.cornerRadius = 20
        label.clipsToBounds = true
        label.layer.borderWidth = 3
        label.layer.borderColor = UIColor.darkGray.cgColor
        label.backgroundColor = .white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 55)
        return label
    }()

    let inputImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 20
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 3
        imageView.layer.borderColor = UIColor.darkGray.cgColor
        return imageView
    }()

    let lineWidth: CGFloat = 14
    var path = UIBezierPath()
    var isCompound = true
    var timer: Timer?

    let predictor = DigitPredictor()

    let inputVC = InputViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Digit Classifier"
        navigationItem.largeTitleDisplayMode = .always
        view.backgroundColor = UIColor(named: "SkyBlue")

        view.addSubview(inputImageView)
        inputImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        inputImageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        inputImageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        inputImageView.widthAnchor.constraint(equalTo: inputImageView.heightAnchor).isActive = true

        view.addSubview(prediction)
        prediction.widthAnchor.constraint(equalTo: prediction.heightAnchor).isActive = true
        prediction.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        prediction.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
        prediction.heightAnchor.constraint(equalTo: inputImageView.heightAnchor).isActive = true

        view.addSubview(canvas)
        canvas.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        canvas.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16).isActive = true
        canvas.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        canvas.bottomAnchor.constraint(equalTo: inputImageView.topAnchor, constant: -20).isActive = true

        let pan = UIPanGestureRecognizer(target: self, action: #selector(draw))
        canvas.addGestureRecognizer(pan)
        canvas.lineWidth = lineWidth

        let tap = UITapGestureRecognizer(target: self, action: #selector(showInput))
        inputImageView.addGestureRecognizer(tap)

        navigationController?.navigationBar.barStyle = .blackOpaque
        navigationController?.navigationBar.barTintColor = UIColor(named: "Coral")
    }

    @objc private func draw(pan: UIPanGestureRecognizer) {
        switch pan.state {
        case .began:
            if !isCompound {
                path = UIBezierPath()
                isCompound = true
            }
            path.move(to:  pan.location(in: canvas))
            canvas.path = path
            timer?.invalidate()
        case .changed:
            path.addLine(to: pan.location(in: canvas))
            canvas.path = path
        default:
            timer = Timer.scheduledTimer(
            withTimeInterval: 1.5,
            repeats: false) { [weak self] _ in
                DispatchQueue.main.async {
                    self?.isCompound = false
                    self?.makePrediction()
                }
            }
        }
    }

    @objc func showInput() {
        present(inputVC, animated: true)
    }

    // Todo: other API for predictor
    private func makePrediction() {
        let preprocessor = DigitPreprocessor(image: pathImage)
        preprocessor.run()
        let number = predictor.predict(pathImage)
        prediction.text = "\(number)"
        inputImageView.image = preprocessor.inputImage
        inputVC.image = preprocessor.inputImage
    }

    private var pathImage: UIImage {
        let x = path.bounds.origin.x - lineWidth
        let y = path.bounds.origin.y - lineWidth
        let height = path.bounds.height + 2*lineWidth
        let width = path.bounds.width + 2*lineWidth
        let rect = CGRect(x: x, y: y, width: width, height: height)
        let renderer = UIGraphicsImageRenderer(bounds: rect)
        return renderer.image { _ in path.stroke() }
    }

}

