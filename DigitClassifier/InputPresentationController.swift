//
//  InputPresentationController.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 7/9/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class InputPresentationController: UIPresentationController {

    let chrome = UIView()

    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        chrome.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapDismiss))
        chrome.addGestureRecognizer(tap)
    }

    @objc func tapDismiss() {
        presentingViewController.dismiss(animated: true)
    }

    override var frameOfPresentedViewInContainerView: CGRect {
        let x = (containerView!.bounds.width - 280) / 2
        let y = (containerView!.bounds.height - 280) / 2
        return CGRect(x: x, y: y, width: 280, height: 280)
    }

    override func presentationTransitionWillBegin() {
        chrome.frame = containerView!.bounds
        chrome.alpha = 1.0
        containerView?.insertSubview(chrome, at: 0)
    }
}
