# Architecture and meta parameters from
# https://github.com/fchollet/keras/blob/master/examples/mnist_mlp.py

import numpy as np
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop
import coremltools

np.random.seed(42)
batch_size = 128
n_classes = 10
epochs = 20

(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(60000, 784).astype('float32') / 255
X_test = X_test.reshape(10000, 784).astype('float32') / 255

y_train = keras.utils.to_categorical(y_train, n_classes)
y_test = keras.utils.to_categorical(y_test, n_classes)

model = Sequential()
model.add(Dense(512, activation='relu', input_shape=(784,)))
model.add(Dropout(0.2))
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(10, activation='softmax'))

model.summary()

model.compile(
    loss='categorical_crossentropy',
    optimizer=RMSprop(),
    metrics=['accuracy'])

history = model.fit(
    X_train, y_train,
    batch_size=batch_size,
    epochs=epochs,
    verbose=1,
    validation_data=(X_test, y_test))

score = model.evaluate(X_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

coreml_model = coremltools.converters.keras.convert(
    model,
    "image",
    "probabilities")
coreml_model.author = "Tobias Ostner"
coreml_model.short_description = "Classifies handwriten numbers from 0 to 9"
coreml_model.input_description["image"] = "Vector with 784 columns of grayscale values between 0 and 1 inclusive"
coreml_model.output_description["probabilities"] = "Vector of probabilities for the numbers from 0 to 9"
coreml_model.save('NNClassifier.mlmodel')
