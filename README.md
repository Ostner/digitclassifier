# DigitClassifier

Tells you which number you (really) have written. Uses custom classifier and coremltools.

![Demo](Resources/DigitClassifier.gif)
